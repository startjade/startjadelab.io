---
title: StartJade
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

Welcome !

This little project's goal is to give you : 

 * A simple way to launch your [JADE](https://jade-project.gitlab.io) multi-agent platform from source code.
 * A few examples that illustrate the basis of Agents' implementation as introduced in this [JADE's introduction](https://jade-project.gitlab.io/docs/Jade-multiagent-platform_Principles-and-main-functionalities_Herpson-2023.pdf).


Indeed, using the JADE's documentation, you can hardly find how to launch a platform outside the console. This project only aims is thus to show you how simple it is to create and launch some agents.

This project is developped in Java and packaged for the Eclipse IDE; it requires Maven and jdk >1.7 


<!-- * : A Multi-Agents platform!->


<!--
{::comment}
This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
{:/comment}
!-->
