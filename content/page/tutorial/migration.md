# How to make one agent migrate from one container to another (intraplatform)

Your agent and its content MUST be Serializable.

Example 3 illustrates the way for agents to migrate.

## The agent should implement the beforeMove() and afterMove() methods

```java
MyAgentA extends Agent{

  protected void setup(){
   super.setup(); 
   addBehaviour(new BeA(this));
  }

  protected void beforeMove(){ 
   super.beforeMove(); 
   // e.g. kill Gui 
  }

  protected void AfterMove(){ 
   super.afterMove(); 
   addBehaviour(
   new BehaviourX()); 
   //e.g. restart Gui 
  }}

```

## The agent calls the doMove() method.

```java
BeA extends OneShotBehaviour {
  public BehaviourA(Agent a){
    //Init
  }
  
  public void action(){
   // Whatever your agent shoud do, then :
   ContainerID cID= new ContainerID();
   cID.setName("AnotherContainer1"); //Destination container
   cID.setAddress(ip); //IP of the host of the container
   cID.setPort(port); //port associated with Jade
   this.myAgent.doMove(cID);// LAST method to call in a behaviour, ALWAYS
  } 
}
```
The doMove() method MUST be the last method called in the behaviour.