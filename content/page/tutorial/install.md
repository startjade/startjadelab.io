# How to install and Execute the project

### Install Java and Eclipse

1. [JDK 8 or more recent](https://www.oracle.com/java/technologies/javase-downloads.html)
2. [Eclipse IDE for Java developpers](http://www.eclipse.org/downloads/packages/). 

### Download and install the project

* Using Eclipse:
 1. File/Import/Git/Projects from Git/ Clone URI/
 2. Paste in the URI field : https://gitlab.com/startjade/startJade.git
 3. Next/Next/Next/Next/Finish
 4. Your program is ready to be launched.

 The alternative is for you to download it manually then to import the project in the IDE :

* Using git manually : git clone https://gitlab.com/startjade/startJade.git
* Downloading the archive from this page (https://gitlab.com/startjade/startJade/-/archive/master/startJade-master.zip)

### Launch the program 

* Using Eclipse : Select the Principal.java file inf the princ package, Right-Click on it and select _run as../Java application_.

The example 2 is then automatically trigerred.

> Note that the lasts version of Eclipse (from 2019-12 to 2020-X) contain by default a Gradle plugin that generates a bug when you try to run the project "model not available for StartJade". If you encounter this:
> 1. Right-click on your project
> 2. Configure
> 3. Add Gradle Nature (the elephant)
> 4. Wait a few seconds for the system to update the project then run the program. Enjoy :)

### Understand what is going on

An example is then automatically trigerred.

Two new frames will appear : One is the rma and the other is the sniffeur. At the same time, you will see in the console several messages :

    INFOS: Service jade.core.event.Notification initialized
    févr. 06, 2016 1:24:04 PM jade.core.PlatformManagerImpl localAddNode 
    INFOS: Adding node <Container-3> to the platform
    févr. 06, 2016 1:24:04 PM jade.core.PlatformManagerImpl$1 nodeAdded
    INFOS: --- Node <Container-3> ALIVE ---
    févr. 06, 2016 1:24:04 PM jade.core.AgentContainerImpl joinPlatform
    INFOS: --------------------------------------
    Agent container Container-3@127.0.0.1 is ready.
    --------------------------------------------
    Launching containers done
    Launching the rma agent on the main container ...
    Launching  Sniffer agent on the main container...
    Plaform ok
    Launching agents for example 2 ...
    AgentA launched on Mycontainer2
    AgentSUM launched on Mycontainer2
    Agents launched...
    Press a key to start the agents -- this action is here only to let you activate the sniffer (see documentation)

*DO NOT PRESS A KEY NOW, OR YOU WILL NOT SEE ANYTHING IN THE JADE's GUI*

In all examples, the "pause" when launching the platform  is indeed here to give you the possibility to activate the sniffer agent from its GUI in order to sniff the agents and to graphically see the message passing process. In any case, I choose here to print the message sent/received on the standard output.

The rma gives you the platform details. You can unfold the AgentPlarforms item to see the differents containers and the created agents. The sniffeur agent's goal is to sniff the messages exchanged among the agents.
For sake of clarity in the source code, I do not activate the sniffer and let you do it from the graphical interface of the sniffeur.

1. In the sniffeur frame , unfold the _AgentsPlatforms_ and the containers.
1. Then, right click on _AgentA_ and select _Do snif_ this agent. A new box will appear in the sniffeur frame.
1. Do the same for the other agent.

You can then select the console and press any key. You will see the message passing both in the console and in the sniffeur's GUI.

You can choose the example to trigger by changing the value of the _EXAMPLE_TO_RUN_ variable in the _Principal.java_ class. 
See the [Presentation](https://startjade.gitlab.io/page/startjade/presentation/) for a quick overview of available examples.
