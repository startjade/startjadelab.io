---
title: About StartJade
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

The StartJade project is used within the [master Androide](http://androide.lip6.fr/) of [Sorbonne University](http://www.sorbonne-universites.fr/), Paris, France, since 2016.

In the course [FoSyMa](http://androide.lip6.fr/?q=node/21) (from the French "Fondement des Systèmes Multi-Agents"), it allows students to discover JADE main's caracteristcs before moving to the core subject, the [Dedale's project](https://dedale.gitlab.io).

It is initiated and maintained by Cédric Herpson from the MAS group of the LIP6 computer science lab.
