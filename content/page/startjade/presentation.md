
# Start Jade

As this project is intended for Licence/Master students, we assume a minimum level in Computer Science :

 -  You know Java or at least an object oriented language,
 -  You know what are a [thread](https://en.wikipedia.org/wiki/Thread_(computing)), a [critical section](https://en.wikipedia.org/wiki/Critical_section) and [serializable objects](https://en.wikipedia.org/wiki/Serialization).
 - You have already heard of JADE. Otherwise, see this [Jade Introduction](http://www-desir.lip6.fr/~herpsonc/wp-content/uploads/2021/02/Jade-multiagent-platform-Principles-and-main-functionalities-Herpson-2021.pdf) and look at [Jade's documentation](https://jade.tilab.com/documentation/tutorials-guides/) for more details.

## Three examples and a half are currently available :

 - **Half-example : How to create a platform**
   - 1 main container, 3 containers
   - DF, AMS, Sniffer

   The main class is the *Principal.java* file. It will show you how to deploy a jade platform. 
   The others implement the differents types of agents and their respectives behaviours. 
   For sake of simplicity, the creation of the containers and of the agents is hardcoded, and I do not activate the sniffer by default. It is bad.


 - **Example 1 : How to create agents and send a message**
   - 1 main container, 3 containers, 4 agents
   - Agent0 on container1
   - Agent1 and Agent3 on container2
   - Agent2 on container3
   - Agent0 will send one message to agent1, that's all :)
    
{{< figure src="/img/sendMessage.png" >}}

<!--
```mermaid
sequenceDiagram
Agent0 ->> Agent1: <Inform,Data>
```
!-->

  
 - **Example 2 : How to exchange messages between two agents**
   - 1 main container, 3 containers, 2 agents
   - AgentA and AgentSUM on container2
   - AgentA will send 10 random values to agentSUM, which will compute the sum and return the result to agentA.

{{< figure src="/img/Sum.png" >}}

<!--
```mermaid
sequenceDiagram
Note left of AgentA: SendNbValues()
AgentA ->> AgentSum: <Inform,val1>
Note right of AgentSum: SumNbReceivedValues()
AgentA ->> AgentSum: <Inform,val2>
AgentA ->> AgentSum: <Inform,val3>
AgentA ->> AgentSum: <Inform,val4>
AgentA ->> AgentSum: <Inform,val5>
AgentA ->> AgentSum: <Inform,val6>
AgentA ->> AgentSum: <Inform,val7>
AgentA ->> AgentSum: <Inform,val8>
AgentA ->> AgentSum: <Inform,val9>
AgentA ->> AgentSum: <Inform,val10>
AgentSum ->>AgentA:<Inform,Result>
Note left of AgentA: ReceivedMsg()
```
!-->

 - **Example 3 : How to migrate from one container to another**
   -  1 main container, 3 containers, 1 agent
   - Agent Oscillator on container2
   - Agent Oscillator moves from container2 to container 1 then oscillates for eternity between containers 1 and 3. It does nothing else.


## [How to install the project ?](https://startjade.gitlab.io/page/tutorial/install/)
 
## License

This program is Free Software: You can use, study share and improve it at your
will. You can also propose new examples for me to integrate them in the project.
You can redistribute and/or modify it under the terms of the GNU Lesser General Public License, either version 3 or any later version.
