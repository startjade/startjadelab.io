# How to create a platform from source-code

## **General principle**

When you create a jade instance, you should at least create a main-container and the agents that manage the platform. From there you can create one or several containers, and deploy agents.
The main steps are :

```java
public static void main(String[] args){
	Runtime rt = Runtime.instance();

	// 1) create a platform (main container+DF+AMS)
	Profile pMain = new ProfileImpl(PLATFORM_IP, PLATFORM_PORT, PLATFORM_ID);
	System.out.println("Launching a main-container..."+pMain);
	AgentContainer mainContainerRef = rt.createMainContainer(pMain); //DF and AMS are include

	// 2) (optionnal) create one or several containers
	String containerName;
	ProfileImpl pContainer;
	System.out.println("Launching containers ...");

	containerName="Mycontainer1";
	pContainer = new ProfileImpl(PLATFORM_IP, PLATFORM_PORT, PLATFORM_ID);
	pContainer.setParameter(Profile.CONTAINER_NAME,containerName);
	System.out.println("Launching container "+pContainer);
	containerRef = rt.createAgentContainer(pContainer); 
	
	// 3) (optionnal) create monitoring agents : rma agent, used to debug and monitor the platform; sniffer agent, to monitor communications; 
	createMonitoringAgents(mainContainerRef);

	System.out.println("Plaform ok");

	//4 ) (optionnal) Create and start your agents
	createMyAgent(containerWhereToCreateMyAgent);
}
```

## **Create a platform distributed over the network**

You want the containers that compose the platform to be distributed over several computers, e.g. A and B.

> Be sure that A and B can communicate with each-others, and that the port you intend to use is open. It is frequent that firewalls or network configuration errors prevent this example to work correctly when you have no experience with distributed app. 

1. On computer A : You need to at least start the main-container. To this aim, just create a code similar to the above example.
1. On computer B : You need to create at least one container by giving it a reference to the main-container living on computer A (IP, Port, Hostname). That's it.

## ** Example source code **
A complete example is available in the [Platform.java](https://gitlab.com/startjade/startJade/-/blob/master/StartJade/src/main/java/princ/Principal.java) file that is used to trigger the various provided algorithms within Start-Jade.

#### If you want to create a platform locally
 1. Just select the _create_platform_ example in _Principal.java_
```java
private static EXAMPLE EXAMPLE_TO_USE=EXAMPLE.FUNDAMENTALS_CREATE_PLATFORM;
```
2. Run the main


#### If you want to create a distributed plaform

__On computer A__ : 

 - Set the platform parameters (IP, port, platform ID), 
 - select the _create_platform_ example in _Principal.java_ and run it.

__ On computer B__ : 

   -  Set the platform parameters to the same value as computer A (IP, port, platform ID), 

   -  Set the COMPUTERisMAIN parameter in _Principal.java_  to *FALSE* (to let it know that there should be no main-computer here),
```java
   public static boolean COMPUTERisMAIN= false;
```
   -  Select the _create_platform_ example in _Principal.java_ and run it.

You should see 4 containers from the AMS. 3 are local to computer A, the last one is connected from B. You can manually migrate the empty agent deployed on A to B then back through the RMA-GUI to insure that all network permissions are ok.





