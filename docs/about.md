# About

The StartJade project was initiated by [Cédric Herpson](https://lip6.fr/cedric.herpson) from the MAS group of the [LIP6 computer science lab](https://www.lip6.fr/?LANG=en), Paris, France.


It's an open-source library that offers :

  *  A simple way to launch your [JADE](https://jade-project.gitlab.io/) multi-agent platform from source code.
  *  A few examples that illustrate the basis of Agents' implementation.
  *  A growing set of standard multi-agents protocols : auction, consensus, gossip,..

It is currently used within the [master Androide](http://androide.lip6.fr/) of [Sorbonne University](http://www.sorbonne-universites.fr/). In the course [FoSyMa](http://androide.lip6.fr/?q=node/21) (from the French "Fondement des Systèmes Multi-Agents"), it allows students to discover MAS and JADE main's caracteristics and classical protocols before moving to the core subject, the [Dedale's project](https://dedale.gitlab.io).

--

## Authors 
  * Cédric HERPSON (Associate professor, SU)

## Main contributors 

 * Roza AMOKRANE (Student, SU)
 * Nabila OULDBELKACEM (Student, SU)
 * Axel FOLTYN (Student, SU)
