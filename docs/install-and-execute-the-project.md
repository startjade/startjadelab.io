# *Install and Execute the project*

## **Install Java and Eclipse**

1. [JDK 14 or more recent](https://www.oracle.com/java/technologies/javase-downloads.html)
2. [Eclipse IDE for Java developpers](http://www.eclipse.org/downloads/packages/). 

## **Download and install the project**

Using Eclipse:

 1. _File_>_Import_>_Git_>_Projects from Git_> _Clone URI_
 2. Paste in the URI field : [https://gitlab.com/startjade/startJade.git](https://gitlab.com/startjade/startJade.git) 
 3. _Next_>_Next (select the master branch)_>_Next_>_Next_>_Finish_
 4. Eclipse automatically build the project, wait for it to finish (bottom right).
 5. Your program is ready to be launched.

The alternative is for you to download it manually then to import the project in the IDE :

* Using git manually : git clone https://gitlab.com/startjade/startJade.git
* Downloading the archive from this page (https://gitlab.com/startjade/startJade/-/archive/master/startJade-master.zip)

## **Launch the program**

We currently only offer to launch the program through the IDE.

* Using Eclipse : Select the Principal.java file inf the princ package, Right-Click on it and select _run as../Java application_.

The example illustrating agent-communication is then automatically trigerred.

### __How to select the example I want to trigger ?__

In the _Principal.java_ class, lines 103 and 109 are the choice you can make. You can either :

 * select an example from one of those available in the EXAMPLE enum,
 * or select EXAMPLE.PROTOCOL_FROM_FILE and then uncomment the json file you want to use.

```java
// Example to be triggered - SELECT IT HERE
private static EXAMPLE EXAMPLE_TO_USE=EXAMPLE.FUNDAMENTALS_COMMUNICATION;
//private static EXAMPLE EXAMPLE_TO_USE=EXAMPLE.PROTOCOLS_GOSSIP_LNS; 
//private static EXAMPLE EXAMPLE_TO_USE=EXAMPLE.CONSENSUS_BASICPAXOS;
//private static EXAMPLE EXAMPLE_TO_USE=EXAMPLE.PROTOCOL_FROM_FILE;

// Used to select the json-file describing the protocol if you selected EXAMPLE.PROTOCOL_FROM_FILE in the above variable
private static String PROTOCOL_FILE = "resources/testEnglishAuction.json";
//private static String PROTOCOL_FILE = "resources/testJapaneseAuction.json";
//private static String PROTOCOL_FILE = "resources/testDutchAuction.json";
```


### __Understand what is going on__

When an example is trigerred, two new frames will appear : One is the rma and the other is the sniffeur. At the same time, you will see in the console several messages :

    Launching containers done
    Launching the rma agent on the main container ...
    Launching  Sniffer agent on the main container...
    Plaform ok
    ---
    Launching agents for example Fundamentals>Communication ... 
    --- 
    Agent0 launched on Mycontainer1
    Agent1 launched on Mycontainer2
    Agent3 launched on Mycontainer2
    Agent2 launched on Mycontainer3
    Agents launched...
    Press enter in the console to start the agents -- this action is here only to let you activate the sniffer (see documentation)


*DO NOT PRESS A KEY NOW, OR YOU WILL NOT SEE ANYTHING IN THE JADE's GUI*

In all examples, the "pause" when launching the platform  is indeed here to give you the possibility to activate the sniffer agent from its GUI in order to sniff the agents and to graphically see the message passing process. In any case, I choose here to print the message sent/received on the standard output.

The rma gives you the platform details. You can unfold the AgentPlarforms item to see the differents containers and the created agents. The sniffeur agent's goal is to sniff the messages exchanged among the agents.
For sake of clarity in the source code, I do not activate the sniffer and let you do it from the graphical interface of the sniffeur.

1. In the sniffeur frame , unfold the _AgentsPlatforms_ and the containers.
1. Then, right click on _AgentA_ and select _Do snif_ this agent. A new box will appear in the sniffeur frame.
1. Do the same for the other agent.

You can then select the console and press any key. You will see the message passing both in the console and in the sniffeur's GUI.