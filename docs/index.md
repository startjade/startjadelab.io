# **Welcome to Start-Jade**

It's an open-source library that offers :

 * A simple way to launch your [JADE](https://jade-project.gitlab.io/) multi-agent platform from source code.
 * A few examples that illustrate the basis of Agents' implementation.
 * A growing set of standard multi-agents protocols : message-passing, auction, consensus and information broadcast/aggregation

Feel free to contribute by proposing a standard protocol's implementation.

---

This project is developped in Java and packaged for Eclipse IDE; it requires Maven and jdk >1.7

---
