# *Behaviours*

<span style="color:blue"> All the examples presented here are available and executable in the start-jade project. </span>

Jade offers several types of behaviours to develop your agents.
You can add/remove one or several behaviour(s) to your agent to change its capabilities.

 * __Classical behaviours__ : Simple, One-shot/Waker and Cyclic/Ticker
 * __Advanced behaviours__ : Sequential, FSM and Parallel ones
 * __Multi-threaded behaviours__ : Threaded version of the previous ones.

All of them contain an _action()_ method that will be automatically called when the behaviour is given the possibility to execute.

---
## **Classical behaviours**

Classical behaviours are variations around the _simpleBehaviour_ class.

With the _action()_ method comes a _done()_ method. The implementation of _done()_  is what will create a difference between classical behaviours.

### **Simple behaviour**

_done()_ is automatically called after each execution of the action() method of the behaviour. If done() return true, the behaviour is removed from the list of behaviour the agent possess. While done() returns false, the associated behaviour stays in the list of the triggerable behaviours of the agent.

![Simple behaviour](/img/simplebehaviour.png)

### __One-shot and Cyclic behaviours__

* A one-shot behaviour is a specialised version of simple-behaviour where _done()_ is not available and always returns true. It is thus executed only once before beeing removed.
* A cyclic behaviour, on the contrary, is a non-terminable behaviour with _done()_ always returning false.

![One-shot and Cyclic behaviours](/img/oneshotCyclicBehaviours.png)

### __Waker and Ticker behaviours__

Waker and Ticker are the temporised versions of the one-shot and cyclic behaviours.

* A waker starts only after k ms (a constructor parameter) and executes only once
* A ticker behaviour will execute itself indefinitely with a minimum periodicity of k ms.

![Waker and Ticker behaviours](/img/wakerTickerBehaviours.png)


---

## **Advanced behaviour**


Any behaviour can be dynamically added to an agent. It it thus possible to ordonate the availability of several behaviours within an agent over time by adding/removing them at the right moment.

The code can neverthess become quite difficult to develop, read and maintain if you do that at a large scale. Advanced behaviours help you to organise your code in this context.

### __Sequential behaviour__

Through the use of subBehaviours, it allows to easily sequence several behaviours

![Sequential behaviour](/img/sequentialBehaviour.png)

### __Finite State Machine behaviour__

Automata are one of the most efficient way to develop complex behaviours. You just need to define the states, their associated behaviours, and the transitions conditions.

![Fsm behaviour](/img/fsm1.png)

![Fsm behaviour](/img/fsm2.png)

### __Parallel behaviour__

Be careful, the name "Parallel behaviour" is misleading. Jade's parallel behaviour does **NOT** allow to execute different behaviours in different threads. 

There is still only one thread that control the agent's behaviours.

The idea behind what Jade's dev called a parallel behaviour is a behaviour composed of a set of sub-behaviours trigerable at the same time (as when you add several behaviours to your agent) but the main behaviour can be instructed to terminate when :

 * ALL of its sub-behaviours have completed or, 
 * ANY sub-behaviour completes


---

## **Multi-threaded behaviour**

By default, 1 agent = 1 thread. 

But you can decide to let your behaviours live in their own threads if they, for example, execute a long and blocking task (gui,computation,..) 

When adding your behaviour to your agent, you only need to __wrap()__ it through the use of a _ThreadedBehaviourFactory()_

See the source-code for the associated example


---

All these examples are extracted from this [Jade's Introduction](http://www-desir.lip6.fr/~herpsonc/wp-content/uploads/2021/02/Jade-multiagent-platform-Principles-and-main-functionalities-Herpson-2021.pdf) and are available [here](https://gitlab.com/startjade/startJade).
