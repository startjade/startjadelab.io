# How to make one agent migrate from one container to another (intraplatform)

In Jade, agents can migrate between platforms and containers using weak migration (code and data, but not the program counter). To allow that an agent and its content **MUST** be Serializable.


## Implement beforeMove() and afterMove()

These methods will be automatically called respectively before and after the migration.

```java
MyAgentA extends Agent{

  protected void setup(){
   super.setup(); 
   addBehaviour(new BeA(this));
  }

  protected void beforeMove(){ 
   super.beforeMove(); 
   //terminate what is computer-specific
   this.terminateLocalElements()// e.g. kill Gui 
  }

  protected void AfterMove(){ 
   super.afterMove(); 
   //restart the agent with the behaviour X
   addBehaviour(new BehaviourX()); 
   // (re)create element that are computer-specific
   this.createLocalElements()//e.g. restart Gui 
  }}

```

## Call doMove() 

The agent defines the destination. Here we set a specific container within the platform.

```java
BeA extends OneShotBehaviour {
  public BehaviourA(Agent a){
    //Init
  }
  
  public void action(){
   // Whatever your agent shoud do, then :
   ContainerID cID= new ContainerID();
   cID.setName("AnotherContainer1"); //Destination container
   cID.setAddress(ip); //IP of the host of the container
   cID.setPort(port); //port associated with Jade
   this.myAgent.doMove(cID);// LAST method to call in a behaviour, ALWAYS
  } 
}
```
The doMove() method **MUST** be the last method called in the behaviour.
