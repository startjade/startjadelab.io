# Communication

 - Generally speaking, the sender agent will, within one of its behaviour :

```java
//1° Create the message
ACLMessage msg=new ACLMessage(ACLMessage.INFORM);//FIPA performative to choose
//2° set the sender
msg.setSender(this.myAgent.getAID());
//3° set the receiver(s)
msg.addReceiver(new AID("ReceiverName",AID.ISLOCALNAME));
msg.addReceiver(new AID("ReceiverName2",AID.ISLOCALNAME));
//4° set the messages caracteristic(s), not mandatory
msg.setProtocol("UselessProtocol");
msg.setOntology("MyOntology");
//5° set the content of the message, either a String OR a serializable object
msg.setContent("Hello World");
//msg.setContentObject(/Serializable object/); You can either use setContent or setContentObject, not both

send(msg);
```


 - While the receiver will :

```java
//1° Create a filter
 MessageTemplate template= MessageTemplate.and(
  MessageTemplate.MatchProtocol("UselessProtocol"),
   MessageTemplate.or(
    MessageTemplate.MatchPerformative(ACLMessage.INFORM),
    MessageTemplate.MatchPerformative(ACLMessage.REFUSE)
   )
  );

  //2° Check the mailbox to see if a message that matches the above template is available
 ACLMessage msg=this.myAgent.receive(template); 
 if (msg!=null){
   //3.a The message is extracted from the mailbox and processed
   String textMessage=msg.getContent()
   //Myobject o = (Myobject) msg.getContentObject(); // If the content is an object, we cast it before using it
 } else{
     //3.b The behaviour goes to sleep, until a new message arrives in the mailbox
  block();
 }
```

See the source code and slides 7 to 9 in this [Jade's Introduction](http://www-desir.lip6.fr/~herpsonc/wp-content/uploads/2021/02/Jade-multiagent-platform-Principles-and-main-functionalities-Herpson-2021.pdf) for more details regarding message passing.
