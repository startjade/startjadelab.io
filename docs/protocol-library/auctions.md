# Single-good auctions

Auctions are mechanisms for allocating goods. In the most simple types of auctions there is one good for sale, one seller, and multiple potential buyers. Each buyer has his own valuation for the good, and each wishes to purchase it at the lowest possible price. 

More details : 

  - Yoav Shoham,Kevin Leyton-Brown (2009) [Multi Agent Systems:Algorithmic, Game-Theoretic,and Logical Foundations](http://www.masfoundations.org/mas.pdf), Chapter 11.


## English auction
- The auctioneer starts the auction by announcing a starting price for the good 
- Each buyer begins to announce their bids; each bid must be superior to the previous one with a minimum increment fixed by the seller
- The conditions to close auction vary, it may end at fixed time or after a long period during when no new bid was made  
- The winner is the agent with the highest bid, he must purchase  the good at a price equal to his last bid





```mermaid
sequenceDiagram
    Auctioneer->>Bidder1: start(objectId, starting price, minimum increment)
    Auctioneer->>Bidder2: start(objectId, starting price, minimum increment)
    Bidder1->>Auctioneer: propose(objectId,price1)
    note right of Bidder1: price1 >= starting price + minimum increment
    Auctioneer->>Bidder2: proposedBy(objectId,price1,Bidder1) 
    Bidder2->>Auctioneer: propose(objectId,price2)
    note right of Bidder2: price2 >= price1 + minimum increment
    Auctioneer->>Bidder1: proposedBy(objectId,Bidder2,price2) 
    note right of Auctioneer: A reservePrice is a minimum price that a auctioneer would be willing to accept from a bidder.
    note right of Auctioneer: wait for a fixed period
    alt if price2 >= reservePrice
    Auctioneer->>Bidder2: accept offer
    Auctioneer->>Bidder1 :end(Bidder2,price2)
    else if price2 < reservePrice
    Auctioneer->>Bidder1: Cancel(objectId)
    Auctioneer->>Bidder2 :Cancel(objectId)
    end
   
    
```


#### Reverse auction
- An auction that has one buyer and many sellers .
- The bidders announce decreasing prices to be able to sell their product to the buyer 
- The price of bids decreases until the good can be sold. 

## Dutch auction
The auctioneer begins with a high asking price and continues to announce  lower prices successively as long as it has not reached reserve price. Dutch auction has also been called a clock auction because in practice the prices are indicated by clock that all the buyers can see. When an agent decides to accept the price, he signals the seller by pressing a buzzer and stopping the clock and the auction ends. This agent must purchase the good for the displayed price.The auction may ends when the reserve price is reached and no buyer has said yes .
```mermaid
sequenceDiagram
    Auctioneer->>Bidder1: start(objectId,starting price)
    Auctioneer->>Bidder2: start(objectId,starting price)
    note right of Auctioneer: A reservePrice is a minimum price that a auctioneer would be willing to accept from a bidder.
    note right of Auctioneer: price1 : is price proposed by Auctioneer which is less than a starting price 
    alt if price1 >= reservePrice
    Auctioneer->>Bidder1: announce(objectId,price1)
    Auctioneer->>Bidder2: announce(objectId,price1)
    end
      note right of Auctioneer: price2 : is price proposed by Auctioneer which is less than a price 1
    alt if price2 >= reservePrice
    Auctioneer->>Bidder1: announce(objectId,price2)
    Auctioneer->>Bidder2: announce(objectId,price2)
    end
    Bidder2->>Auctioneer: YES
    note right of Bidder2 : Bidder2 purchase the object
    Auctioneer->>Bidder2 :Confirm(objectId,price2)
    Auctioneer->>Bidder1 :end(objectId,Bidder2,price2)
    
```

#### Reverse auction
- The buyer specifies a starting price 
- The start price keeps on increasing until a seller places a bid 
- The buyer purchase the good for the displayed price 

## Japanese auction
- The auctioneer displays the initial price for the good
- All buyers that are interested in buying the good at the displayed price, choose to be "in"
- The seller continues successively to  increase the price
- Each agent must announce whether he is still in the auction or not
- No exiting agent can reenter the auction 
- The auction ends when a single agent remains in
- The agent must purchase the good for the current price 

```mermaid
sequenceDiagram
    Auctioneer->>Bidder1: start(objectId, starting price)
    Auctioneer->>Bidder2: start(objectId, starting price)
    Bidder1->>Auctioneer: in
    Bidder2->>Auctioneer: in
    Auctioneer->>Bidder1: announce(objectId,price1)
    Auctioneer->>Bidder2: announce(objectId,price1)
    Bidder1->>Auctioneer: in
    Bidder2->>Auctioneer: in
    Auctioneer->>Bidder1: announce(objectId,price2)
    Auctioneer->>Bidder2: announce(objectId,price2)
    Bidder2->>Auctioneer: in
    Bidder1->>Auctioneer: out
    note right of Bidder2 : Bidder2 purchase the object
    Auctioneer->>Bidder2 :Confirm
```


#### Reverse auction
- The buyer begins with a starting price and continues successively to decrease the price
- At each step, each seller wanting to continue in the auction, must choose to be "in"
- If a seller exits the auction, it can't reenter 
- The auction ends when a single agent  remains in
- The agent must seal the good for the current price
