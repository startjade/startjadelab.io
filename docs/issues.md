# Issue or Feature proposal

You want to add a well-known algorithm to startjade, correct one, or inform us of an issue ? 

 * let us know here on [discord, category Start-jade, channel #dev](https://discord.com/invite/GTwcuaGE74)  or,
 * create an issue on [gitlab](https://gitlab.com/startjade/startJade/issues/new)

Thanks !

