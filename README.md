
This is the Start-Jade's website. A static website used to present Jade's examples.

It is build using [mkDocs](https://www.mkdocs.org/) with the [material](https://github.com/squidfunk/mkdocs-material) theme. 

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

---

## Building and preview locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install python, pip, then mkdocs and material : `pip install mkdocs` , `pip install mkdocs-material`
1. In the terminal, go the project repository and run `mkdocs serve`
1. Add content, save it
1. It will automatically be updated and can be accessed under http://127.0.0.1:8000
1. commit and push/request merge when ready

---

### How to create your own webpage using Gitlab pages

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equaly if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].